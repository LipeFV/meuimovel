<?php

namespace App\Http\Controllers\Api\User;

use App\ApiMessages\ApiMessages;
use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class UserController extends Controller
{
    private $user;

    public function __construct(User $user)
    {
        $this->user = $user;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = $this->user->paginate('10');

        return response()->json($users, 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();
        $data['password'] = bcrypt($data['password']);

        if (!$request->has('password') || !$request->get('password')) {
            $message = new ApiMessages('É necessário informar uma senha para o usuário');
            return response()->json([$message->getMessage()], 401);
        }

        Validator::make($data, [
            'profile.phone' =>  'required',
            'profile.mobile_phone' =>  'required',
        ])->validate();

        try {
            $user = $this->user->create($data);

            /**
             * Salvando o profile do usuário
             */
            $profile = $data['profile'];
            $profile['social_networks'] = serialize($profile['social_networks']);

            $user->profile()->create($profile);

            return response()->json([
                'data'   =>  [
                    'message'   =>  'Usuário cadastrado com sucesso!',
                    'status'    =>   '201'
                ]
            ]);
        } catch (\Exception $ex) {
            $message = new ApiMessages($ex->getMessage());
            return response()->json([$message->getMessage()]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            $user = $this->user->with('profile')->findOrFail($id);
            $user->profile->social_networks = unserialize($user->profile->social_networks);

            return response()->json([
                'data'   => $user,
                'status'    =>   '200'
            ]);
        } catch (\Exception $ex) {
            $message = new ApiMessages($ex->getMessage());
            return response()->json([$message->getMessage()]);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = $request->all();

        if ($request->has('password') && $request->get('password')) {
            $data['password'] = bcrypt($data['password']);
        } else {
            unset($request['password']);
        }

        Validator::make($data, [
            'profile.phone' =>  'required',
            'profile.mobile_phone' =>  'required',
        ])->validate();

        $profile = $data['profile'];
        $profile['social_networks'] = serialize($profile['social_networks']);

        $user->profile()->create($profile);

        try {
            $user = $this->user->findOrFail($id);
            $user->update($data);

            $user->profile()->update($profile);

            return response()->json([
                'data'   =>  [
                    'message'   =>  'Usuário Atualizado com sucesso!',
                    'status'    =>   '200'
                ]
            ]);
        } catch (\Exception $ex) {
            $message = new ApiMessages($ex->getMessage());
            return response()->json([$message->getMessage()]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $user = $this->user->findOrFail($id);
            $user->delete();

            return response()->json([
                'data'   =>  [
                    'message'   =>  'Usuário Removido com sucesso!',
                    'status'    =>   '200'
                ]
            ]);
        } catch (\Exception $ex) {
            $message = new ApiMessages($ex->getMessage());
            return response()->json([$message->getMessage()]);
        }
    }
}
