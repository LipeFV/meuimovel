<?php

namespace App\Http\Controllers\Api\RealState;

use App\ApiMessages\ApiMessages;
use App\Http\Controllers\Controller;
use App\Http\Requests\RealStateRequest;
use App\Models\RealState;
use Illuminate\Http\Request;

class RealStateController extends Controller
{
    private $realState;

    public function __construct(RealState $realState)
    {
        $this->realState = $realState;
    }
    /***
     * Index
     */
    public function index()
    {

        $realState = auth('api')->user()->realState();

        return response()->json($realState->paginate(), 200);
    }

    /**
     * Show 
     */
    public function show($id)
    {
        try {

            $realState = auth('api')->user()->realState()->with('realStatePhoto')->findOrFail($id);

            return response()->json([
                'data'   => $realState,
            ], 200);
        } catch (\Exception $ex) {
            $message = new ApiMessages($ex->getMessage());
            return response()->json([$message->getMessage()]);
        }
    }

    /**
     * Store
     */
    public function store(RealStateRequest $request)
    {
        $data = $request->all();
        $images = $request->file('images');
        try {

            $data['user_id'] = auth('api')->user->id;

            $realState = $this->realState->create($data);

            /**
             * Salvando categorias
             */
            if(isset($data['categories']) && count($data['categories'])){
                $realState->categories()->sync($data['categories']);
            }
            /**
             * Upload de imagens
             */
            if($images){

                $imagesUploaded = [];

                foreach ($images as $image) {
                    $path = $image->store('images', 'public');
                    $imagesUploaded[] = [
                        'photo' => $path,
                        'is_thumb'  =>  false
                    ];
                }

                $realState->realStatePhoto()->createMany($imagesUploaded);
            }
            return response()->json([
                'data'   =>  [
                    'message'   =>  'Imóvel cadastrado com sucesso!',
                    'status'    =>   '201'
                ]
            ]);
        } catch (\Exception $ex) {
            $message = new ApiMessages($ex->getMessage());
            return response()->json([$message->getMessage()]);
        }
    }

    /**
     * Update
     */
    public function update($id, RealStateRequest $request)
    {
        $data = $request->all();
        $images = $request->file('images');
        try {

            dd($data);

            //$realState = $this->realState->findOrFail($id);

            $realState = auth('api')->user()->realState()->findOrfail($id);
            $realState->update($data);
            
             /**
             * Upload de imagens
             */
            if($images){

                $imagesUploaded = [];

                foreach ($images as $image) {
                    $path = $image->store('images', 'public');
                    $imagesUploaded[] = [
                        'photo' => $path,
                        'is_thumb'  =>  false
                    ];
                }

                $realState->realStatePhoto()->createMany($imagesUploaded);
            }
            
            if(isset($data['categories']) && count($data['categories'])){
                $realState->categories()->sync($data['categories']);
            }
            
            return response()->json([
                'data'   =>  [
                    'message'   =>  'Imóvel Atualizado com sucesso!',
                    'status'    =>   '201'
                ]
            ]);
        } catch (\Exception $ex) {
            $message = new ApiMessages($ex->getMessage());
            return response()->json([$message->getMessage()]);
        }
    }
    /***
     * Delete
     */
    public function destroy($id)
    {

        try {
            $realState = auth('api')->user()->realState()->findOrFail($id);

            $realState->delete();

            return response()->json([
                'data'   =>  [
                    'message'   =>  'Imóvel Removido com sucesso!',
                    'status'    =>   '200'
                ]
            ]);
        } catch (\Exception $ex) {
            $message = new ApiMessages($ex->getMessage());
            return response()->json([$message->getMessage()]);
        }
    }
}
