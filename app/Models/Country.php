<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Country extends Model
{
    public function states()
    {
        $this->hasMany(State::class, 'country_id', 'id');
    }
}
