<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class UserProfile extends Model
{
    protected $table = 'user_profiles';

    protected $fillable = [
        'phone', 
        'mobile_phone',
        'about',
        'social_networks',
        'user_id',
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
