<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\RealStatePhoto;
use App\User;

class RealState extends Model
{
    protected $appends = ['_links'];

    protected $fillable = [
        'user_id',
        'title',
        'description',
        'content',
        'price',
        'bathorroms',
        'bedrooms',
        'property_area',
        'total_property_area',
        'slug'
    ];

    public function getLinksAttribute()
    {
        return [
          'href'    =>  route('real-states.real-states.show', ['realState' => $this->id]),
          'rel' => 'Imóveis'  
        ];
    }
    
    /**
     * Um imovel pertence a um usuário
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }
    /**
     * Um imovel tem muitas categirias
     */
    public function categories()
    {
        return $this->belongsToMany(Category::class,'real_state_categories');
    }

    /***
     * Um imovel tem muitas fotos
     */
    public function realStatePhoto()
    {
        return $this->hasMany(RealStatePhoto::class, 'real_state_id', 'id');
    }

    public function adresse()
    {
        return $this->hasOne(Adress::class);
    }
}
