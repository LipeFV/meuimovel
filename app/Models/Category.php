<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $fillable = [
        'name',
        'description',
        'slug'
    ];

    public function realState()
    {
        return $this->belongsToMany(realState::class,'real_state_categories');
    }
}
