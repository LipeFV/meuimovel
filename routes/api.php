<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::prefix('v1')->namespace('Api')->group(function () {

    /**
     * Rotas para autenticação
     */
    Route::post('login', 'Auth\\LoginJWTController@login')->name('login');
    Route::get('logout', 'Auth\\LoginJWTController@logout')->name('logout');
    Route::get('refresh', 'Auth\\LoginJWTController@refresh')->name('refresh');
    /**
     * Rotas para pesquisar de imoveis
     */
    Route::get('search','Search\\RealStateSearchController@index')->name('search.index');
    Route::get('search/show/{id}','Search\\RealStateSearchController@show')->name('search.show');

    Route::group(['middleware' => 'jwt.auth'], function () {

        Route::name('real_states.')->namespace('RealState')->group(function () {
            Route::resource('real-states', 'RealStateController');
        });

        /**
         * Rotas para Usuários
         */
        Route::name('users.')->namespace('User')->group(function () {
            Route::resource('users', 'UserController');
        });
        /**
         * Rotas para Categorias
         */
        Route::name('categories.')->namespace('Category')->group(function () {
            Route::get('categories/{id}/real-state', 'CategoryController@getRealState')->name('real.state.category');
            Route::resource('categories', 'CategoryController');
        });
        /***
         * Rotas para Fotos
         */
        Route::name('photos.')->prefix('photos')->namespace('PhotoRealState')->group(function () {
            Route::delete('/delete/{id}', 'RealStatePhotoController@remove')->name('remove');
            Route::put('/set-thumb/{photoId}/{realStateId}', 'RealStatePhotoController@setThumb')->name('set.thumb');
        });
    });
});
